#ifndef QuadrupedGait_h
#define QuadrupedGait_h

#include <Arduino.h>
#include <inttypes.h>
#include <StateMachine.h>
#include <BaBauHound.h>
#include <LegMovementManager.h>
#include <TrajectoryPlanner.h>
#include <Queu.h>

#define Rx 60 //Stroke
#define Height 20 //Ground Clearence Heigh
#define Floor 155

#define MidDistance 0
#define FrontSW Rx/2
#define RearSW -Rx/2

#define NumberOfLegs 4
#define MaxMovementVelocity 0.5 //	m/seg
#define DefaultAngleVelocity 0.05

#define MovementResolution 3 //mm
#define AngleResolution MovementResolution*180/(PI*165)


typedef float (*TrajectoryFunction)(float);
typedef unsigned char uint8_t;
typedef uint8_t byte;
typedef uint8_t (*PointerToFunction)();

class GaitPlanner
{
	public:
		void Runner(bool,float);
		Quadruped * Osqar;
		float MovementVelocity;
		float AngleVelocity;
		TrajectoryFunction LegTrajectory[4];
		
		StateMachine STM;
		State Start;
		State MoveCOG;
		State Pause;
		State AssureStaticStability;
		bool GaitPause;
		
		int Selector;	
		int LegCounter;	

		int XLegOffset[4];
		int YLegOffset[4];
		
		void SetOffsetXY(int,int,int);
		
		LegMovementManager LegManager;
		Leg * MovingLeg;		
};

class TwoPhasesDiscontinious: public GaitPlanner
{
	public:
		void Initialize(Quadruped *);
		
	private:
	//States
		State RearRightLeg;
		State RearLeftLeg;		
		State FrontRightLeg;
		State FrontLeftLeg;

		State MoveRightCOG;
		State MoveLeftCOG;
		
	//Starting State
		//uint8_t DoEnterSF();
		//uint8_t StartFunction();
		
	//Moving RearLeg State
		//uint8_t DoEnterRLF();
		//uint8_t RearLegFunction();
		
	//Moving FrontLeg State
		//uint8_t DoEnterFLF();
		//uint8_t FrontLegFunction();
	
	//Moving COG State
		//uint8_t DoEnterCOG();
		//uint8_t MoveCOGFunction();
		
	//Pause State
		//uint8_t PauseFunction();
		
	//Assuring Static Stability State
		//uint8_t DoEnterASSF();
		//uint8_t AssureStaticStabilityFunction();			
	
	//for every state, any of these functions will be assigned to LegTrajectory[i]	
		float MidDistanceTrajectory(float);
		float PropusliveTrajectory(float);
		void FrontContact(float);	
};

#endif

