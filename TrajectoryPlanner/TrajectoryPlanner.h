#ifndef TrajectoryPlanner_h
#define TrajectoryPlanner_h

#include <Arduino.h>
#include <inttypes.h>
#include <Queu.h>

/***************** In order to avoid using dynamic memory *******************/
  #define NumberOfSamples 10
/****************************************************************************/
#define MaxSampleSize 300
#define True 1
#define False 0
#define AccelerationFactor 0.1
#define CubicTimeFactor 1000
#define LSPB FALSE

#define ElbowDown 0
#define ElbowUp 1
#define HipLink 75
#define KneeLink 90
#define AnkleLink 40

/*
	TrajectoryPlanner - CLASS
		TrajectorySampler (for XY(t) Ankle Positions)
		InverseKinematics Module (for each sampled point)
		Interpolation Module (for each joint - Hip and Knee [Angles])
*/

class TrajectoryPlanner
{
	public:				
		/*Stays*/
		//Constructor & Setup
		void Initialize();
		void NewTraject(float,float,float,float,float,int);	//Use it with TrajectoryRunner()
		
		//Destroyer
		void DeleteTraject();//

		//Runner
		void Runner();
		
		//TrajectorySampler
		void SetJointArray(int);
		bool InverseKinematics(float, float, int);
		
		//Cubic Spline
		void CubicSpline(float *, float *);
		float CubicSplineRunner(float *, float);

		int TrackHandler(float);
		float TimeHandler(float);
		int TrackNumber;
		bool FinishedTrajectory;
		bool Running;
		
		//Parameters		
		float InitialPositionX;		
		float InitialPositionY;		
		float FinalPositionX;		
		float FinalPositionY;
		
		float InverseKinematicHip;
		float InverseKinematicKnee;
		
		float FinalTime;
		float TrackTime;
		float InitialTime;
		//int TrackNumber;
		
	/*			Sampler Parameters		*/
        float HipArray[NumberOfSamples+1];
        float KneeArray[NumberOfSamples+1];
		
	/*			Constants				*/
		float HipConstants[4*(NumberOfSamples)];
		float KneeConstants[4*(NumberOfSamples)];
		
	/*			Hip & Knee Values		*/
		float Hip;
		float Knee;
		
	private:

};

#endif