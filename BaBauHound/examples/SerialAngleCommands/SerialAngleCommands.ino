#include <BaBauHound.h>
#include <TrajectoryPlanner.h>
#include <Queu.h>

#define HipP 6
#define KneeP 10
#define AHipCorrect -15
#define AKneeCorrect -20
#define DHipCorrect 0
#define DKneeCorrect -50

Leg LegA,LegD;

#define MoveRate 2000
float LastTime,ActualTime;
float AHipAngle;
float AKneeAngle;
float DHipAngle;
float DKneeAngle;
int i=0;

int Selector;

void setup()
{
  Serial.begin(57600);
  while(Serial.available()<=0)
  {
  }
  LegA.Create(HipP,KneeP,0,0,ElbowUp);
  LegA.InvertHip=False;
  LegA.InvertKnee=False;
  LegA.SetJointOffset(AHipCorrect,AKneeCorrect);
  
  LegD.Create(HipP+1,KneeP+1,0,0,ElbowUp);
  LegD.InvertHip=True;
  LegD.InvertKnee=True;
  LegD.SetJointOffset(DHipCorrect,DKneeCorrect);  
  
  LastTime=0;
}

void loop()
{
  if(Serial.available()>0)
  {
    Selector=Serial.parseInt();
    if(Selector==255)
    {
      AHipAngle=Serial.parseInt();
      AKneeAngle=Serial.parseInt();
      DHipAngle=Serial.parseInt();
      DKneeAngle=Serial.parseInt();
      
      LegA.MoveHip(AHipAngle);
      LegA.MoveKnee(AKneeAngle);
      LegD.MoveHip(DHipAngle);
      LegD.MoveKnee(DKneeAngle);      
      
      Serial.print("\nAHip: ");
      Serial.print(AHipAngle,DEC);
      Serial.print(" ");
      Serial.print(LegA.ReadHip(),DEC);      
      Serial.print("\nAKnee: ");
      Serial.print(AKneeAngle,DEC);
      Serial.print(" ");
      Serial.print(LegA.ReadKnee());      
      
      Serial.print("\nDHip: ");
      Serial.print(DHipAngle,DEC);
      Serial.print(" ");
      Serial.print(LegD.ReadHip(),DEC);      
      Serial.print("\nDKnee: ");
      Serial.print(DKneeAngle,DEC);
      Serial.print(" ");
      Serial.print(LegD.ReadKnee());
    }
    else
      Serial.parseInt();    
  }
}