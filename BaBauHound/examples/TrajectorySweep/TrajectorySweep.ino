#include <BaBauHound.h>
#include <TrajectoryPlanner.h>
#include <Queu.h>
#include <DifferentialADC.h>
#include <LabViewProtocol.h>

ADCManager ADManager;
DifferentialADC ADC1,ADC2;

#define HipP 6
#define KneeP 10
#define DHipCorrect 0
#define DKneeCorrect 0

Leg LegD;
Queu SQueu;

#define MoveRate 2000
#define SampleRate 10

float LastTime,ActualTime;
float HipAngle;
int i=0;

float PositionX=-60;
float PositionY=135;
float Velocity=0.075;
float Acceleration=4*Velocity;

void setup()
{  
  Serial.begin(57600);  
  SerialInitialize(&SQueu);  
  
  ADManager.Initialize();
  ADC1.Attach(&ADManager);
  ADC2.Attach(&ADManager);
  
  LegD.Create(HipP,KneeP,DHipCorrect,DKneeCorrect);
  LegD.InvertHip=False;
  LegD.InvertKnee=False;
  
  LastTime=0;
  LegD.Homming(ElbowDown);
  LegD.NewMovement(PositionX,PositionY,Velocity,Acceleration,ElbowDown);  
}

void loop()
{
  ADManager.Runner();
  LegD.Move();
  
  switch(i%2)
  {
    case 0:
      PositionX=-20;
      PositionY=135;
    break;
    case 1:
      PositionX=-114;
      PositionY=14;
    break;
    default:
      PositionX=-20;
      PositionY=135;      
  }
    
    if(LegD.FinishedMovement())
    {
      LegD.NewMovement(PositionX,PositionY,Velocity,Acceleration,ElbowDown);
      i++;
    }
    
    ActualTime=millis();
    if(ActualTime-LastTime>=SampleRate)
    {
          LastTime=millis();
          SerialSubscribe(ADC1.ReadConversion()/2,1);
          SerialSubscribe(LegD.ReadHip(),2);
          SerialSubscribe(ADC2.ReadConversion()/2,3);          
          SerialSubscribe(LegD.ReadKnee()/2,4);          

          SerialSample();     
    }
}