/*##################################
**			BaBauHound			  **
##################################*/

/*	UpSide View
		
		Head
	[A]______[B]
	   
	[C]______[D]
	
	
	LSPB Trajectory
	
        Velocity
             |
MaxVelocity  |   __________
             |  /          \
             | /            \
            0|/______________\____time 
             to tb      tf-tb tf	
*/
/*
	Features v1.0
	
	-"Calibrate" motors with constants
	-Able to invert Hip and/or Knee orientation
	-Goes from one point to another using an LSPB trajectory (for every link)
		-Calculates Inverse Kinematic on initial and final points


*/
#include "BaBauHound.h"

/*--------------------------------------------------------------------
----------------------------------------------------------------------
				Quadruped Class
----------------------------------------------------------------------

UpSide View
		
		Head
	[0]______[1]
	   
	[2]______[3]

---------------------------------------------------------------------*/


/*
**--------------------------------------------------------------------

	Pre: QLegs must be a Leg Array size 4.
	
		COG its supossed to be centered
**--------------------------------------------------------------------*/
int HipCorrection[]={-49,38,35,-30};
int KneeCorrection[]={18,-15,-5,17};
bool InvertH[]={False,True,False,True};
bool InvertK[]={False,True,True,False};
bool Elbow[]={ElbowUp,ElbowUp,ElbowDown,ElbowDown};
 void Quadruped::Initialize(/*, QuadrupedGait * GaitType*/)
{
	for(int i=0;i<4;i++)
	{
		Legs[i].Create(HipP+i,KneeP+i,HipCorrection[i],KneeCorrection[i],Elbow[i]);
		Legs[i].InvertHip=InvertH[i];
		Legs[i].InvertKnee=InvertK[i];
	}
	//ImplementedGait=GaitType;
	
	Length=FrontToRearHipDistance;
	FrontWidth=188;
	RearWidth=166;
	
	//Calculating COG Coordinates
	this->RefreshCOGCoordinates();
}


//COG is refered  to Leg 3
//
//	COG and Hip coordinates equivalency 
//
//		1COGX = 1HipX
//		1COGY = -1ZHip
//		1COGZ = 1YHip

void Quadruped::RefreshCOGCoordinates()
{	
	float auxiliarLength,auxiliarFWidth,auxiliarRWidth,auxiliarCOG;
	
	auxiliarLength=Length;
	auxiliarFWidth=FrontWidth;
	auxiliarRWidth=RearWidth;
	
	//Calculating COGX
	auxiliarCOG= -Legs[3].ReadPositionX() + Length/2;
	COGX=auxiliarCOG;
	
	//Calculating COGY
	auxiliarCOG= RearWidth/2;
	COGX=auxiliarCOG;	
	
	//Calculating COGZ
	auxiliarCOG= Legs[3].ReadPositionY();
	COGX=auxiliarCOG;	
}



/*--------------------------------------------------------------------
----------------------------------------------------------------------
				Leg Class
----------------------------------------------------------------------
---------------------------------------------------------------------*/


/*
**--------------------------------------------------------------------
**		Leg CLASS Method Create()
**		PRE:	Digital Pin Number 1-13 - In the leg Kinematics
			straigth arm paralel to	x axis corresponds to Hip=0,
			Knee=0. Hip and Knee both increase counter-clockwise
			Invert -> 1 TRUE
					  0 FALSE
**		POST:	
**--------------------------------------------------------------------
*/
void Leg::Create(int HipPin, int KneePin)
{
		
		HipCorrection=0;//90º as being parallel to the y axis
		KneeCorrection=0;//0º for straigth knee
		InvertHip=FALSE;//Default 0.- Counter-Clockwise
		InvertKnee=TRUE;//Default 1 - Counter-Clockwise
		ElbowUpSelect=ElbowDown;
		/*************************************************************************************
			Could be a method that stops movement and restart all Movement parameters
		*************************************************************************************/
                Traject.Initialize();
        /*************************************************************************************/
		ServoHip.attach(HipPin,MinPulseTime,MaxPulseTime);
		ServoKnee.attach(KneePin,MinPulseTime,MaxPulseTime);

        Hip=this->ReadHip();
        Knee=this->ReadKnee(); 
}


void Leg::Create(int HipPin, int KneePin, int HipCorrect, int KneeCorrect)
{
		
		HipCorrection=HipCorrect;//90º as being parallel to the y axis
		KneeCorrection=KneeCorrect;//0º for straigth knee
		InvertHip=FALSE;//Default 0.- Counter-Clockwise
		InvertKnee=TRUE;//Default 1 - Counter-Clockwise
		ElbowUpSelect=ElbowDown;
		/*************************************************************************************
			Could be a method that stops movement and restart all Movement parameters
		*************************************************************************************/
                Traject.Initialize();
        /*************************************************************************************/
		ServoHip.attach(HipPin,MinPulseTime,MaxPulseTime);
		ServoKnee.attach(KneePin,MinPulseTime,MaxPulseTime);

        Hip=this->ReadHip();
        Knee=this->ReadKnee(); 
}

void Leg::Create(int HipPin, int KneePin, int HipCorrect, int KneeCorrect,bool Elbow)
{
		
		HipCorrection=HipCorrect;//90º as being parallel to the y axis
		KneeCorrection=KneeCorrect;//0º for straigth knee
		InvertHip=FALSE;//Default 0.- Counter-Clockwise
		InvertKnee=TRUE;//Default 1 - Counter-Clockwise
		ElbowUpSelect=Elbow;
		/*************************************************************************************
			Could be a method that stops movement and restart all Movement parameters
		*************************************************************************************/
                Traject.Initialize();
        /*************************************************************************************/
		ServoHip.attach(HipPin,MinPulseTime,MaxPulseTime);
		ServoKnee.attach(KneePin,MinPulseTime,MaxPulseTime);

        Hip=this->ReadHip();
        Knee=this->ReadKnee();

		//Update Hip And Knee Value
		this->UpdateHipByteValue();
		this->UpdateKneeByteValue();
}

/*
**--------------------------------------------------------------------
**		Leg CLASS Method Delete()
**		PRE:	
**		POST:	True when deleted
**--------------------------------------------------------------------
*/
bool Leg::Delete()
{
	if(this->IsActive()) //Invalid posiion - Exist
	{
		ServoHip.detach();
		ServoKnee.detach();
		Hip=0;
		Knee=0;
		this->UpdateHipByteValue();
		this->UpdateKneeByteValue();
		return 1;
	}
	else
		return 0;
}

/*
**--------------------------------------------------------------------
**		Leg CLASS Method IsActive()
**		PRE:	
**		POST:	True when Attached
**--------------------------------------------------------------------
*/
bool Leg::IsActive()
{
	if(ServoHip.attached()||ServoKnee.attached()) //Exist
	{
		return 1;
	}
	else
		return 0;
}

/*
**--------------------------------------------------------------------
**		Leg CLASS Method SetJointOffset(int,int)
**		PRE:	
**		POST:	True when Attached
**--------------------------------------------------------------------
*/

void Leg::SetJointOffset(int HipCorrect,int KneeCorrect)
{
	HipCorrection=float(HipCorrect);
	Serial.print(HipCorrection);
	
	KneeCorrection=float(KneeCorrect);
	Serial.print(KneeCorrection);
}

//Conversion parameters
#define MIN_PULSETIME 800
#define MAX_PULSETIME 2327
#define MIN_ANGLE 0
#define MAX_ANGLE 180

//movement parameters
#define MaxHipAngle 180
#define MinHipAngle 0
#define MaxKneeAngle 160
#define MinKneeAngle 10

/*
**--------------------------------------------------------------------
**		Leg CLASS Method MoveHip()
**		PRE:	Angle in degrees 0-180
**		POST:	1 if valid angle
**--------------------------------------------------------------------
*/
bool Leg::MoveHip(float Angle)
{		
	if(!InvertHip)
	{
		Hip=Angle+HipCorrection;
		
		if(Hip<MinHipAngle)
			Hip=MinHipAngle;
		if(Hip>MaxHipAngle)
			Hip=MaxHipAngle;		
		
		float Microseconds;			
		Microseconds=((Hip-MIN_ANGLE)*(MAX_PULSETIME-MIN_PULSETIME)/(MAX_ANGLE-MIN_ANGLE))+MIN_PULSETIME;
		ServoHip.write(Microseconds);
		Hip=(ServoHip.read()-HipCorrection);
	}
	else
	{
		Hip=180-Angle+HipCorrection;
		
		if(Hip<MinHipAngle)
			Hip=MinHipAngle;
		if(Hip>MaxHipAngle)
			Hip=MaxHipAngle;	
			
		float Microseconds;			
		Microseconds=((Hip-MIN_ANGLE)*(MAX_PULSETIME-MIN_PULSETIME)/(MAX_ANGLE-MIN_ANGLE))+MIN_PULSETIME;
		ServoHip.write(Microseconds);
		Hip=180-((ServoHip.read())-HipCorrection);
	}
	this->ReadHip();
	this->UpdateHipByteValue();
	
	
	return 1;//Valid
}

/*
**--------------------------------------------------------------------
**		Leg CLASS Method MoveKnee()
**		PRE:	Angle in degrees 0-180
**		POST:	1 if valid angle
**--------------------------------------------------------------------
*/
bool Leg::MoveKnee(float Angle)
{
	if(ElbowUpSelect)
		Angle=-Angle;	
	if(!InvertKnee)
    {
       	Knee=Angle+KneeCorrection;//Degrees
		
		if(Knee<MinKneeAngle)
			Knee=MinKneeAngle;
		if(Knee>MaxKneeAngle)
			Knee=MaxKneeAngle;			
		
		float Microseconds;			
		Microseconds=((Knee-MIN_ANGLE)*(MAX_PULSETIME-MIN_PULSETIME)/(MAX_ANGLE-MIN_ANGLE))+MIN_PULSETIME;
		ServoKnee.write(Microseconds);
        Knee=(ServoKnee.read()-KneeCorrection);//Degrees
    }
    else
    {	
        Knee=180-Angle+KneeCorrection;//Degrees
		
		if(Knee<MinKneeAngle)
			Knee=MinKneeAngle;
		if(Knee>MaxKneeAngle)
			Knee=MaxKneeAngle;		
			
		float Microseconds;			
		Microseconds=((Knee-MIN_ANGLE)*(MAX_PULSETIME-MIN_PULSETIME)/(MAX_ANGLE-MIN_ANGLE))+MIN_PULSETIME;
		ServoKnee.write(Microseconds);
        Knee=180-(ServoKnee.read()-KneeCorrection);//Degrees
    }
	this->ReadKnee();
	this->UpdateKneeByteValue();	
	
	return 1;//Valid	
}

/*
**--------------------------------------------------------------------
**		Leg CLASS Method ReadHip()
**		PRE:	
**		POST:	Angle [Min,Max]
**--------------------------------------------------------------------
*/
float Leg::ReadHip()
{
	float Microseconds=ServoHip.readMicroseconds();
	float Angle=((Microseconds-MIN_PULSETIME)*(MAX_ANGLE-MIN_ANGLE)/(MAX_PULSETIME-MIN_PULSETIME))+MIN_ANGLE;		
	if(!InvertHip)
	{
    	Hip=(Angle-HipCorrection);
	}
	else
	{
		Hip=180-((Angle)-HipCorrection);
	}
	
	this->UpdateHipByteValue();
	return Hip;//Valid  
}


/*
**--------------------------------------------------------------------
**		Leg CLASS Method ReadHip()
**		PRE:	
**		POST:	Angle [Min,Max]
**--------------------------------------------------------------------
*/
float Leg::ReadKnee()
{
	float Microseconds=ServoKnee.readMicroseconds();
	float Angle=((Microseconds-MIN_PULSETIME)*(MAX_ANGLE-MIN_ANGLE)/(MAX_PULSETIME-MIN_PULSETIME))+MIN_ANGLE;	
	if(!InvertKnee)
    {
        Knee=(Angle-KneeCorrection);//Degrees
    }
    else
    {
        Knee=180-(Angle-KneeCorrection);//Degrees
    }
	if(ElbowUpSelect)
		Knee=-Knee;
	this->UpdateKneeByteValue();	
	return Knee;//Valid  
}

/*
**--------------------------------------------------------------------
**		Leg CLASS Method ReadPositionX() - Ankle Position
**		PRE:	
**		POST:
**--------------------------------------------------------------------
*/
// #define MIN_PULSETIME 800
// #define MAX_PULSETIME 2200
// #define MIN_ANGLE 0
// #define MAX_ANGLE 165
/************************************************************************
            ONLY FOR TESTS
**************************************************************************/
float Leg::ReadHipMicroseconds()
{
	float Microseconds=ServoHip.readMicroseconds();
	float Angle=((Microseconds-MIN_PULSETIME)*(MAX_ANGLE-MIN_ANGLE)/(MAX_PULSETIME-MIN_PULSETIME))+MIN_ANGLE;
	return Angle;
  //return (Microseconds-MIN_PULSETIME)*((MAX_ANGLE-MIN_ANGLE)/(MAX_PULSETIME-MIN_PULSETIME));
}

float Leg::ReadKneeMicroseconds()
{
	float Microseconds=ServoKnee.readMicroseconds();  
	float Angle=((Microseconds-MIN_PULSETIME)*(MAX_ANGLE-MIN_ANGLE)/(MAX_PULSETIME-MIN_PULSETIME))+MIN_ANGLE;
	return Angle;	
	//return (Microseconds-MIN_PULSETIME)*((MAX_ANGLE-MIN_ANGLE)/(MAX_PULSETIME-MIN_PULSETIME));
}

float Leg::ReadPositionX()
{
	float XPosition;
	Hip=this->ReadHip();
	Knee=this->ReadKnee();
	XPosition=KneeLink*cos((Hip+Knee)*PI/180)+HipLink*cos(PI*Hip/180);
	return XPosition;
}

/*
**--------------------------------------------------------------------
**		Leg CLASS Method ReadPositionY() - Ankle Position
**		PRE:	
**		POST:
**--------------------------------------------------------------------
*/
float Leg::ReadPositionY()
{
	float YPosition;
	Hip=this->ReadHip();
	Knee=this->ReadKnee();
	YPosition=KneeLink*sin((Hip+Knee)*PI/180)+HipLink*sin(PI*Hip/180);
	return YPosition;
}

/*
**--------------------------------------------------------------------
**		Leg CLASS Method Homming()
**		PRE:	
**		POST:Calibrates leg
**				-Hip
**				-Knee
**--------------------------------------------------------------------
*/

void Leg::Homming(int Elbow)
{
  this->NewMovement(0,140,0.1,0.1*0.1,Elbow);
  while(!Traject.FinishedTrajectory)
  {
    this->Move();
  }
}

/*
**--------------------------------------------------------------------
**		v0.a
**		Leg CLASS Method NewMovement()
**		PRE:	XPosition, YPosition (Degrees)
				Velocity [Degrees/s]
				Acceleration [Degrees/s^2]
**		POST:	
**--------------------------------------------------------------------
*/
void Leg::NewMovement(float XFinal, float YFinal, float MaxVelocity, float MaxAcceleration,int Elbow)
{
	if(MaxVelocity<0.01)
		MaxVelocity=0.01;
		
	float Time,XInitial,YInitial;
	XInitial=this->ReadPositionX();
	YInitial=this->ReadPositionY();
	Time=(sqrt(sq(XFinal-XInitial)+sq(YFinal-YInitial)))/MaxVelocity;
        
	Traject.NewTraject(XInitial,YInitial,XFinal,YFinal,Time,Elbow);
}

/*
**--------------------------------------------------------------------
**		v0.a
**		Leg CLASS Method Move()
**		PRE:	Before this function, use NewMovement(Parameters)
**				to setup the movement.
**		POST:	Returns FinishedTraject
**--------------------------------------------------------------------
*/
/*
	Example 1:
	Leg.NewMovement(Parameters);
	while(!Leg.Move)
	{
	}
	
	Example 2:
	Leg.NewMovement(Parameters);
	while(!Leg.FinishedMovement())
	{
		Leg.Move();
	}
*/
bool Leg::Move()
{
	Traject.Runner();
    this->MoveKnee(Traject.Knee);
    this->MoveHip(Traject.Hip);            
	return Traject.FinishedTrajectory;
}

/*
**--------------------------------------------------------------------
**		v0.a
**		Leg CLASS Method FinishedMovement()
**		PRE:	
**		POST:	Returns FinishedTraject
**--------------------------------------------------------------------
*/

bool Leg::FinishedMovement()
{
	return Traject.FinishedTrajectory;
}


//For OsqarProtocol
void Leg::UpdateHipByteValue()
{
	//Updates High and Low
	int AuxiliarData=0;
	AuxiliarData=Hip;
	
	//Update HipH and HipL
	UnsignedHipH=(AuxiliarData>>8)&255;
	UnsignedHipL=AuxiliarData&255;
}

void Leg::UpdateKneeByteValue()
{
	//Updates High and Low
	int AuxiliarData=0;
	AuxiliarData=Knee;
	
	//Update HipH and HipL
	UnsignedKneeH=(AuxiliarData>>8)&255;
	UnsignedKneeL=AuxiliarData&255;
}

uint8_t * Leg::ReadHipHAddress()
{
	return &UnsignedHipH; //returns Array of Pointer to High and Low
}

uint8_t * Leg::ReadHipLAddress()
{
	
	return &UnsignedHipL; //returns Array of Pointer to High and Low
}

uint8_t * Leg::ReadKneeHAddress()
{
	return &UnsignedKneeH; //returns Array of Pointer to High and Low
}

uint8_t * Leg::ReadKneeLAddress()
{
	return &UnsignedKneeL; //returns Array of Pointer to High and Low
}

