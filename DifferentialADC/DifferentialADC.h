/*********************************************************
*
*
*
*
*
*
*
*
*
*/

#include "wiring_private.h"
#include "pins_arduino.h"
#include <Arduino.h>
#include <inttypes.h>
#include <Queu.h>

#ifndef DifferentialADC_h
#define DifferentialADC_h

class ADCManager;
class DifferentialADC;

#define PRESCALER2 0
#define PRESCALER4 1
#define PRESCALER8 2
#define PRESCALER16 3
#define PRESCALER32 4
#define PRESCALER64 5
#define PRESCALER128 6

#define TRUE 1
#define FALSE 0

byte MapOptionToRegister(byte);
byte MapRegisterToOption(byte);

class ADCManager
{
	public:
		void Initialize();
	
	//RUNNER
	void Runner();
		
	//Service Methods		
		void Subscribe(DifferentialADC *);
		void UnSubscribe(DifferentialADC *);		
		
		uint16_t OccupiedChannels; //OccupiedChannel-bits
		uint16_t ConversionReadyRegister;	/************/
		
		int Conversion;
		byte Option;
		Queu PendingConversions;
		uint16_t PendingConversionsRegister;		
		
	private:
	//ADC Methods
		void Setup();
		int ReadConversion();
		short int ReadRegisters();
		bool isConversionRunning();
		
	//Service Methods
		bool isChannelSubscribed(byte);
	
	//Actual Channel Parameters
		// int Conversion;
		// byte Option;
	
	//ChannelConversionManager Methods/Parameters
		void ChannelManager();
		/**/
		void UpdateConversionValue(byte);
		void AttendNextConversion();
		/**/
		void ConversionRequest(byte);
		void SetConversionPending(byte);
		/**/
		void ConversionAttended(byte);
		void ClearConversionPending(byte);
		/**/
		bool isConversionPending(byte);
		/**/
		void SetConversionReadyFlag(byte);
		void ClearConversionReadyFlag(byte);		
		
	//Buffer
		//uint16_t PendingConversionsRegister;
		//Queu PendingConversions;
		DifferentialADC * ConversionBuffer[16]; //Initially all of them are pointing NULL
};

class DifferentialADC
{
	public:
		void Initialize(ADCManager *);
		byte Attach(byte,ADCManager *); //take as a parameter the selected option
		byte Attach(ADCManager *); //returns available attached option
		void Detach();
		
		long ReadConversion(); //number from -512 to 512 (10 bits)
		byte ChannelOption;
		long ChannelConversion; //Timed Conversion	
		
		//For OsqarProtocol
		void UpdateConversionBytesValues();		
		uint8_t * ReadConversionByteHAddress();
		uint8_t * ReadConversionByteLAddress();		

	private:
		void Setup();
		
		bool isChannelAvailable(byte);
		bool isConversionAvailable();
		byte SelectAvailableChannel();
		
		byte ChannelStatus; //1 if ok, 255 if not
		
		ADCManager * ADManager;
		
		//For OsqarProtocol
		uint8_t ConversionHigh;
		uint8_t ConversionLow;
				
};

/*
	Single Conversion Procedure
	
	void setup()
	{
		Setup(parameters);
	
	}
	loop()
	{
		int ADC;
		ADC=ReadConversion();
	}
*/
/*
	ADC Reference Constants
	
	*DEFAULT 1
	*EXTERNAL 0
	*INTERNAL1V1 2
	*INTERNAL2V56 3
	
	ADMUX REGISTER:
	
	REFS1	REFS0	ADLAR	MUX 4	MUX 3	MUX 2	MUX 1	MUX 0
	  7		  6		  5		  4		  3		  2		  1		  0
	  
	
	Reference Selection Table
	REFS1	REFS0		Reference Selection	
	  0		  0				AREF, Internal VREF turned off	  
	  0		  1				AVCC with external capacitor at AREF pin
	  1		  0				Internal 1.1V with external capacitor at AREF pin	  
	  1		  1				Internal 2.56V with external capacitor at AREF pin
*/

/*
	ADC Differential Channel Selection
	
	Channel Selection Table
	Option
	(MUX5:0)
	
	8-15			ADC+		ADC-		Gain
	8				0			0			10x
	9				1			0			10x
	10				0			0			200x
	11				1			0			200x
	12				2			2			10x
	13				3			2			10x
	14				2			2			200x
	15				3			2			200x
	
	16-29			ADC+		ADC-		Gain
	16				0			1			1x
	17				1			1			1x
	18				2			1			1x
	19				3			1			1x
	20				4			1			1x
	21				5			1			1x
	22				6			1			1x
	23				7			1			1x
	24				0			2			1x
	25				1			2			1x
	26				2			2			1x
	27				3			2			1x
	28				4			2			1x
	29				5			2			1x
	
	40-47			ADC+		ADC-		Gain
	40				8			8			10x
	41				9			8			10x
	42				8			8			200x
	43				9			8			200x
	44				10			10			10x
	45				11			10			10x
	46				10			10			200x
	47				11			10			200x
	
	48-61			ADC+		ADC-		Gain
	48				8			9			1x
	49				9			9			1x
	50				10			9			1x
	51				11			9			1x
	52				12			9			1x
	53				13			9			1x
	54				14			9			1x
	55				15			9			1x
	56				8			10			1x
	57				9			10			1x
	58				10			10			1x
	59				11			10			1x
	60				12			10			1x
	61				13			10			1x
*/

/*
	ADC Prescaler Selection
	
	ADCSRA Register:
	
	ADEN	ADSC	ADATE	ADIF	ADIE	ADPS2	ADPS1	ADPS0
	7		6		5		4		3		2		1		0
	
	ADPS2	ADPS1	ADPS0	Division Factor
	0		0		0		2
	0		0		1		2
	0		1		0		4
	0		1		1		8
	1		0		0		16
	1		0		1		32
	1		1		0		64
	1		1		1		128
*/

#endif