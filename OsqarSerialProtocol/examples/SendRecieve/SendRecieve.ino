#include <OsqarSerialProtocol.h>
#include <StateMachine.h>
#include <Queu.h>

Sample TestSample;
Sample SampleArray[7];

TxManager Tx;
RxManager Rx;

float ActualTime=0;
float LastTime=0;
uint8_t TestData[SampleSize];

void setup()
{
  TestSample.Initialize();
  Tx.Initialize();
  Rx.Initialize();
  for(int i=0;i<7;i++)
  { 
    SampleArray[i].Initialize();
  }
  Serial.begin(57600);
  while(Serial.available()<=0)
  {
  }
  
  for(int i=0;i<SampleSize;i++)
  {
    TestData[i]=10*i+20;
    TestSample.Add(&TestData[i]);
  }

  Tx.SubscribeSample(&TestSample);
}

void loop()
{
    Rx.Runner();
    Tx.Runner();
}
