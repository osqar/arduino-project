#include <QuadrupedGait.h>
#include <StateMachine.h>
#include <BaBauHound.h>
#include <LegMovementManager.h>
#include <TrajectoryPlanner.h>
#include <Queu.h>
#include <DifferentialADC.h>
#include <LabViewProtocol.h>

Quadruped Osqar;
TwoPhasesDiscontinious TPD;


float MovementVelocity;
/*****************************************************************/
//SerialRecieve
/*****************************************************************/
#define SerialMaxRecieved 127
#define SerialMinRecieved -127
#define MaxRecievedAngle 160
#define MinRecievedAngle -160

bool IDRecieved=0;
bool FinishedCount=0;
bool FinishedRecieving=0;

uint8_t HeaderCount=0;
uint8_t ByteCount=0;

uint8_t ID=0;
uint8_t ByteArray[8];

uint8_t ActualID=0;
long HighDWord=0;
long LowDWord=0;

StateMachine STM;
State WaitingForID,RecievingData,StoringData;

//only for test

float ActualTime=0;
float LastTime=0;
uint8_t LastID=0;
int ChangeIDTimes=0;

#define SampleTime 5000


//Serial
uint8_t DoEnterWaitingForID()
{
  Serial.print("S");
  
  IDRecieved=0;
  FinishedCount=0;
  ByteCount=0;
  HeaderCount=0;
  FinishedRecieving=0;
  return 0;
}

uint8_t DoWaitingForID()
{
  if(Serial.available()>0)
  {
    unsigned char RecievedASCII=Serial.read();
    Serial.print("W");  
    if(RecievedASCII==128)
    {
      HeaderCount=HeaderCount+1;
      Serial.print(HeaderCount);      
      return 0;
    }
    else
    {
      if((RecievedASCII==0)&&(HeaderCount>=4))
      {
        IDRecieved=1;
        return 2;
      }
      else
        IDRecieved=0;
        return 1;
    }
  }
}

uint8_t DoOutWaitingForID()
{
  HeaderCount=0;
  ByteCount=0;
  return 0;
}

uint8_t DoEnterRecievingData()
{
  IDRecieved=1;
  ByteCount=0;
  FinishedCount=0;
  return 0;
}

uint8_t DoRecievingData()
{
  if(Serial.available()>0)
  {
    if(ByteCount==0)
    {
      ID=Serial.read();
      Serial.print("I");
      Serial.print(ID);
    }
    else
    {
      Serial.print("D");
      ByteArray[ByteCount-1]=Serial.read();
      Serial.print(ByteArray[ByteCount-1]);
    }
    ByteCount++;
//    Serial.print(ByteCount);
    if(ByteCount>8)
    {
      return 2;
    }
    else
    {
      return 0;
    }
  }
  return 0;
}

uint8_t DoOutRecievingData()
{
  Serial.print("F");
  IDRecieved=1;
  FinishedCount=1;
  ByteCount=0;
  HeaderCount=0;
  return 0;
}

uint8_t DoEnterStoringData()
{
  FinishedRecieving=0;
  return 0;
}

uint8_t DoStoringData()
{
  ActualID=ID;  
  
  long AuxiliarDWord=0;
  HighDWord=0;
  LowDWord=0;
  for(int i=0;i<4;i++)
  {
    AuxiliarDWord=ByteArray[i];
    HighDWord=HighDWord|AuxiliarDWord<<(24-(i*8));
    AuxiliarDWord=0;
  
    AuxiliarDWord=ByteArray[i+4];
    LowDWord=LowDWord|AuxiliarDWord<<(24-(i*8));   
  }
  Serial.print("L");
  Serial.print(LowDWord); 
  return 1;
}

uint8_t DoOutStoringData()
{
  FinishedCount=0;
  IDRecieved=0;
  FinishedRecieving=1;
  return 0;
}



void setup()
{
  Osqar.Initialize();
  TPD.Initialize(&Osqar);
  
  Serial.begin(57600);  
  //Setting Up SerialRecieve States
  WaitingForID.Create(&WaitingForID,&RecievingData,DoWaitingForID,DoEnterWaitingForID,DoOutWaitingForID);
  RecievingData.Create(&RecievingData,&StoringData,DoRecievingData,DoEnterRecievingData,DoOutRecievingData);
  StoringData.Create(&WaitingForID,DoStoringData,DoEnterStoringData,DoOutStoringData);
  STM.Initialize(&WaitingForID);
  
}

void loop()
{ 
  //Recieve Protocol
  STM.Runner();
  ActualTime=millis();
  
    switch(ActualID)
    {
      case 250: //Step-By-Step Gait
        uint8_t SBSMask;
        SBSMask=LowDWord&255;
        if(SBSMask==33)
        {
          Serial.write(LowDWord&255);
          TPD.Selector=1; //Read Next Step       
        }               
        
        TPD.Runner(0,0.075);
        TPD.Selector=0; //Don't do any step          
        LowDWord=0; //Clear Next Step        
        
      break;
      case 249: 
        if(ActualTime-LastTime>=SampleTime)
        {
          LastTime=millis();
          TPD.Selector=1;       
        }
        TPD.Runner(0,0.03);
        TPD.Selector=0; //Don't do any step       
      break;
      case 248: //Send Angle
        uint8_t AngleByte;
        float RecievedAngle;
        
        for(int i=0;i<4;i++)
        {
          //HipAngle
          AngleByte=(HighDWord>>(24-8*i))&255;
          RecievedAngle=float(AngleByte);
          if(AngleByte>127) //Negative Angle
          {
            AngleByte=~AngleByte;
            AngleByte=AngleByte+1;
            RecievedAngle=float(AngleByte);
            RecievedAngle=-RecievedAngle;
          }
          RecievedAngle=map(RecievedAngle,SerialMaxRecieved,SerialMinRecieved,MaxRecievedAngle,MinRecievedAngle);
          Osqar.Legs[i].MoveHip(RecievedAngle);
          
          //KneeAngle
          AngleByte=(LowDWord>>(24-8*i))&255;
          RecievedAngle=float(AngleByte);
          if(AngleByte>127) //Negative Angle
          {
            AngleByte=~AngleByte;
            AngleByte=AngleByte+1;
            RecievedAngle=float(AngleByte);
            RecievedAngle=-RecievedAngle;
          }
          RecievedAngle=map(RecievedAngle,SerialMaxRecieved,SerialMinRecieved,MaxRecievedAngle,MinRecievedAngle);
          Osqar.Legs[i].MoveKnee(RecievedAngle); 
        }       
        
      break;
      case 247: //Send Position
      
      break;
      case 246: //Send Angle Correction    
        uint8_t AngleOffsetByte;
        float RecievedAngleOffset,RecievedHipOffset,RecievedKneeOffset;
        
        for(int i=0;i<4;i++)
        {
          //HipAngle
          AngleOffsetByte=(HighDWord>>(24-8*i))&255;
          RecievedAngleOffset=float(AngleOffsetByte);
          if(AngleOffsetByte>127) //Negative Angle
          {
            AngleOffsetByte=~AngleOffsetByte;
            AngleOffsetByte=AngleOffsetByte+1;
            RecievedAngleOffset=float(AngleOffsetByte);
            RecievedAngleOffset=-RecievedAngleOffset;
          }
          RecievedAngleOffset=map(RecievedAngleOffset,SerialMaxRecieved,SerialMinRecieved,MaxRecievedAngle,MinRecievedAngle);
          RecievedHipOffset=RecievedAngleOffset;          
          
          //KneeAngle
          AngleOffsetByte=(LowDWord>>(24-8*i))&255;
          RecievedAngleOffset=float(AngleOffsetByte);
          if(AngleOffsetByte>127) //Negative Angle
          {
            AngleOffsetByte=~AngleOffsetByte;
            AngleOffsetByte=AngleOffsetByte+1;
            RecievedAngleOffset=float(AngleOffsetByte);
            RecievedAngleOffset=-RecievedAngleOffset;
          }
          RecievedAngleOffset=map(RecievedAngleOffset,SerialMaxRecieved,SerialMinRecieved,MaxRecievedAngle,MinRecievedAngle);
          RecievedKneeOffset=RecievedAngleOffset;
          
          Osqar.Legs[i].SetJointOffset(RecievedHipOffset,RecievedKneeOffset); 
        }         
      break;
      case 245: //Send Position Correction
        
        uint8_t AuxiliarByte,AuxiliarNegative;
        float OffsetX,OffsetY;
        
        for(int i=0;i<4;i++)
        {        
          //XOffset
          AuxiliarByte=(HighDWord>>(24-i*8))&255;
          
          OffsetX=float(AuxiliarByte);
          if(AuxiliarByte>127)//Negative
          {
            AuxiliarNegative=(~AuxiliarByte)+1;
            OffsetX=-float(AuxiliarNegative);
          }
          OffsetX=map(OffsetX,127,-128,165,-165);
          
          //Y Offset
          AuxiliarByte=(LowDWord>>(24-i*8))&255;
          
          OffsetY=float(AuxiliarByte);
          if(AuxiliarByte>127)//Negative
          {
            AuxiliarNegative=(~AuxiliarByte)+1;
            OffsetY=-float(AuxiliarNegative);
          }
          OffsetY=map(OffsetY,127,-128,165,-165);
          
          TPD.SetOffsetXY(i,OffsetX,OffsetY);          
        }     
      break;    
      default:
        int j;
        j=0;
    }
    
//    if(ActualID!=LastID)
//    {
//      ChangeIDTimes++;
//      LastID=ActualID;
//      Serial.write(LowDWord);
//      Serial.write(uint8_t(ActualID));
//      Serial.write(ChangeIDTimes+48);
//    }
}