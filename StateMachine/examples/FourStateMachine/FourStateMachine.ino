#include "StateMachine.h"
#include <stdio.h>
#include <Arduino.h>
//if any state doesn't return anything else than 0, StateMachine will act like a loop

State FirstState,SecondState,ThirdState,FourthState;
StateMachine STM;
int i=0;

uint8_t Enter()
{
  Serial.print("\n\nEntrando al Estado por primera vez");
  return 0;
}

uint8_t Out()
{
  Serial.print("\n\nSaliendo del Estado");
  return 0;
}

uint8_t FirstStateFunction()
{
    Serial.print("\nThis is State 1");	    
    while(Serial.available()<=0)
    {
    }
    int Select;
    Select=Serial.read()-48;
    return Select;
}

uint8_t SecondStateFunction()
{
    int Select=0;
    Serial.print("\nThis is State 2");
    return 1;
}

uint8_t ThirdStateFunction()
{
    Serial.print("\nThis is State 3");
    return 1;
}

uint8_t FourthStateFunction()
{
     Serial.print("\nThis is last state");
     while(Serial.available()<=0)
     {
     }
     Serial.read();
     i=0;
     
     return 1;
}

void setup()
{      
  Serial.begin(57600);
  
  FirstState.Create(&SecondState,&FourthState,FirstStateFunction,Enter,Out);
  SecondState.Create(&ThirdState,&FirstState,SecondStateFunction,Enter,Out);
  ThirdState.Create(&FourthState,ThirdStateFunction,Enter,Out);
  FourthState.Create(&FirstState,FourthStateFunction,Enter,Out);
	
  STM.Initialize(&FirstState);
}

void loop()
{
  STM.Runner();
}